// selecting input and h3 field
var inp = document.getElementById("inp");
var h3 = document.getElementById("h3");
// selecting table
var table = document.getElementById("table");
// function to create table
function showTable() {
    // handling error in input if input is not number
    if (isNaN(parseFloat(inp.value))) {
        h3.innerHTML = "Please enter the number input";
    }
    else {
        // if user enters zero
        if (parseFloat(inp.value) === 0) {
            table.innerHTML = '';
            h3.innerHTML = "enter number other than 0";
        }
        else {
            var input_num = parseInt(inp.value);
            // reseting table if user enters another number
            table.innerHTML = '';
            h3.innerHTML = '';
            for (var i = 1; i <= input_num; i++) {
                //creating row for table
                var row = table.insertRow();
                //creating cell for the row
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(0);
                //inserting values to the cell
                cell1.innerHTML = "" + i * input_num;
                cell2.innerHTML = "" + i + " * " + input_num + " = ";
            }
        }
    }
}
//# sourceMappingURL=script.js.map