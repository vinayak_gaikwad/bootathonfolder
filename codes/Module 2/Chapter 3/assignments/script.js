//selecting required field to collect values and show result
var x1 = document.getElementById("x1");
var y1 = document.getElementById("y1");
var x2 = document.getElementById("x2");
var y2 = document.getElementById("y2");
var x3 = document.getElementById("x3");
var y3 = document.getElementById("y3");
var x = document.getElementById("x");
var y = document.getElementById("y");
var ans = document.getElementById("ans");
// function to calculate the distance between two coordinates
function distance(p, q, r, s) {
    var dist = Math.sqrt(Math.pow((p - r), 2) + Math.pow((q - s), 2)); //by the distance formula
    return dist;
}
// function to calculate the area of triangle by Heron's formula
function areaOfTriangle(a, b, c, d, e, f) {
    // distance between the two coordinates of triangle
    var d1 = distance(a, b, c, d);
    var d2 = distance(a, b, e, f);
    var d3 = distance(c, d, e, f);
    // calculating semiperimeter s
    var s = (d1 + d2 + d3) / 2;
    var area = Math.sqrt(s * (s - d1) * (s - d2) * (s - d3)); //implementation of Heron's formula
    return area;
}
// function to verify the condition and to show result
function result() {
    // verification if input value is number or not
    if (isNaN(parseFloat(x1.value)) || isNaN(parseFloat(y1.value)) || isNaN(parseFloat(x2.value)) ||
        isNaN(parseFloat(y2.value)) || isNaN(parseFloat(x3.value)) || isNaN(parseFloat(y3.value)) ||
        isNaN(parseFloat(x.value)) || isNaN(parseFloat(y.value))) {
        ans.innerHTML = "Please enter number as input";
    }
    else {
        // calculating the area of all possible triangles from given points
        var abc = areaOfTriangle(x1.value, y1.value, x2.value, y2.value, x3.value, y3.value);
        var abd = areaOfTriangle(x1.value, y1.value, x2.value, y2.value, x.value, y.value);
        var adc = areaOfTriangle(x1.value, y1.value, x.value, y.value, x3.value, y3.value);
        var dbc = areaOfTriangle(x.value, y.value, x2.value, y2.value, x3.value, y3.value);
        console.log(abc, abd, adc, dbc, abd + adc + dbc);
        //condition for determining the point lies inside or outside  of the triangle
        if (abc.toFixed(3) === (abd + adc + dbc).toFixed(3)) { //toFixed is used to round off upto 3 decimal places
            ans.innerHTML = "Point lies inside the triangle"; // result
        }
        else {
            ans.innerHTML = "Point does not lies inside the triangle";
        }
    }
}
//# sourceMappingURL=script.js.map