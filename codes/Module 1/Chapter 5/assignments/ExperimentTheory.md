# Instrumentation and Working Principles of Infra Red (IR) Spectroscopy Using Salt Plates

When two atoms combine to form a stable covalent moleculer, there are two repulsion forces acting between the two hetero atoms. One between the positively charged nuclei of both the atoms and the other between the negative electron clouds. The other force is the attraction between the nucleus of one atom with the electons of the other atom. Balancing the forces between them, the two atoms settle at a mean internuclear distance or the bond length where the total energy of the system is minimum. Any change like pulling the atoms away or squeezing them brings change in the bond length which requires an input of energy.

A diatomic molecule with the above description is considered as two vibrating masses that are connected by a spring. The internuclear distance between the atoms at energy minimum is referred to as the equilibrium distance (re)Any change in this distance is given by Hooke' law as:

$$f = {-k(r - re)}.$$


where f is the restoring force and r is the bond length. The energy associated cinsidering the energy curve to be parobolic is given as :

$$E = \frac{1}{2} K (r-r_e)^2.$$


![Vibrational Energy Levels](image1.png)

Figure 2: Vibrational energy levels and allowed transitions between them for a diatomic molecule in a simple harmonic motion.
[Picture source](http://condensedconcepts.blogspot.in/2011/10/concrete-test-of-morse-potential-in.htm)

For any haormonic oscillator, when the bond vibrates, its enrgy of vibration is changing continually and periodically from kinetic to potential enrgy and back again. The total energy is proportional to the frequency of vibration, and is given as:

$$E_o_s_c = h\nu_o_s_c.$$


 The elastic nature of the bond has an intrinsic vibrational frequency which is determined by the force constant K of the spring or its stiffness, the masses of the bonded atoms.

 $$v = \frac{1}{2\pi c}\sqrt\frac{k}{\mu}.$$

 where c is the speed of light, and μ is the reduced mass of the system which is given by:

 $$\mu = \frac{(m_1)(m_2)}{m_1} + m_2.$$

 The value of the force constant varies from bond to bond. K for a triple bond is three times those of a single bond and for a double bond it is twice those of a single bond. There are two significant features that can be drawn:

 1. Strong bonds have large force constants and vibrate at a higher frequency than the weaker bonds.
 2. Bonds between the heavy atoms (larger reduced mass) vibrate at a lower frequency than that of bonds between the lighter atoms.

![image2](image2.png)

Figure 3: Vibrational modes of an organic molecule.The energy of molecular vibration is quantised. A molecule can only stretch and bend at certain allowed frequencies.
[Picture source](http://chemwiki.ucdavis.edu/Organic_Chemistry/Organic_Chemistry_With_a_Biological_Emphasis/Chapter__4%3A_Structure_Determination_I/Section_4.2%3A__Infrared_spectroscopy)

### Sample preparation for Infrared spectroscopy:

 The sample to be measured must be taken in a sample holder or cell which is made of ionic substance. Sodium chloride or potassium bromide plates are used. Potassium bromide plates are more expensive than sodium chloride but has an advantage over the sodium chloride because of the asbsorbs at 650cm-1. Glass or platics absorb strongly through the infrared region.

 ### Liquids:

 A drop of the liquid sample is placed between a pair of polished IR salt plates. The plates are pressed together and the sample forms a thin film between the salt plates. A spectrum obtained by this method is neat for there is no use of solvent in the sample preparation.

 ### Solids:

 One of the methods used to prepare sample is the Bbr Pellet method. Where the sample is powdered along with KBr and pressed under high pressure to form a pellet.

The other method is the Nujol method where the sample is ground along with few drops of mineral oil or nujol and the thick suspension is place between the salt plates.

![image6](image6.jpg)

Figure 4: IR salt plates (Sodium Chloride)
[Picture source](http://www.sciencephoto.com/media/222708/view)

### The Infrared Spectrometer:

 The infrared spectrometer or the spectrophotometer is the instrument that determines the IR absorption spectra of a compound. There are two types of spectrometers that are widely used in laboratories.
 
 1. Dispersive infrared spectrometers
 2. Fourier-Transform spectrometers

![image7](image7.jpg)

Figure4: The main components of a fourier transform infrared (FTIR) spectrophotometer.
[Picture source](http://cnx.org/content/m34660/latest/?collection=col10699/latest)


FT-IR spectrometers are the modern spectometers that provide the spectrum more rapidly than the dispersive ones. The optical pathway produces a pattern called the interferogram. This is a complex signal which is a plot of intensity versus time. For more practical purposes this time-domine spectrum is converted to a frequency-domain spectrum, that is intensity versus frequency. This conversion is done by a mathematical operation called a Fourier-Transform (FT). FT separates the individual frequencies from the interferogram producing a virtual spectrum identical to the one abtained form dispersive spectrometers. The biggest advantage of using a FT-IR spectometers is that it is possible to collect number of interferograms of the same sample in less than a second. When a fourier-Transform is performed a sum of all the interferograms gives spectrum which has a better signal to noise ratio. Greater speed and greater sensitivity makes FT-IR spectrometers preferred over dispersive spectometers.

  


