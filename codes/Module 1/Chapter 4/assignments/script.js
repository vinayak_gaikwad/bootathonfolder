//selecting all recquired input and buttons by IDs
var si1 = document.getElementById("si1");
var si2 = document.getElementById("si2");
var b1 = document.getElementById("b1");
var sui1 = document.getElementById("sui1");
var sui2 = document.getElementById("sui2");
var b2 = document.getElementById("b2");
var mi1 = document.getElementById("mi1");
var mi2 = document.getElementById("mi2");
var b3 = document.getElementById("b3");
var di1 = document.getElementById("di1");
var di2 = document.getElementById("di2");
var b4 = document.getElementById("b4");
var s = document.getElementById("s");
var su = document.getElementById("su");
var m = document.getElementById("m");
var d = document.getElementById("d");
// Function to add two numbers
function add() {
    if (isNaN(parseFloat(si1.value)) || isNaN(parseFloat(si2.value))) { // to check the enter value is number or not
        s.innerHTML = "Please Enter the Number as input";
    }
    else {
        var ans_s = parseFloat(si1.value) + parseFloat(si2.value);
        s.innerHTML = ans_s.toString(); // displaying result on screen
    }
}
// Function to subtract two numbers
function sub() {
    if (isNaN(parseFloat(sui1.value)) || isNaN(parseFloat(sui2.value))) { // to check the enter value is number or not
        su.innerHTML = "Please Enter the Number as input ";
    }
    else {
        var ans_sub = parseFloat(sui1.value) - parseFloat(sui2.value);
        su.innerHTML = ans_sub.toString(); // displaying result on screen
    }
}
// Function to multiply two Numbers
function mul() {
    if (isNaN(parseFloat(mi1.value)) || isNaN(parseFloat(mi2.value))) { // to check the enter value is number or not
        m.innerHTML = "Please Enter the Number as input";
    }
    else {
        var ans_mul = (parseFloat(mi1.value)) * (parseFloat(mi2.value));
        m.innerHTML = ans_mul.toString(); // displaying result on screen
    }
}
// Function for division
function div() {
    if (isNaN(parseFloat(di1.value)) || isNaN(parseFloat(di2.value))) { // to check the enter value is number or not
        d.innerHTML = "Please Enter the Number as input";
    }
    else {
        if (parseFloat(di2.value) === 0) { // handling division by zero
            d.innerHTML = "Math Error Cannot divide by zero";
        }
        else {
            var ans_div = parseFloat(di1.value) / parseFloat(di2.value);
            ans_div = Math.round(ans_div * 1000) / 1000; // rounding off the large floating point value to the three digit
            d.innerHTML = ans_div.toString(); // displaying result on screen   
        }
    }
}
//# sourceMappingURL=script.js.map